 import {Direction} from "../enums/robot"
 // Define the robot state
 
 export interface Robot {
  x: number;
  y: number;
  direction: Direction;
}