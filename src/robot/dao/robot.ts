import * as fs from "fs";
import { Command } from "../enums/robot";
import { Robot } from "../interfaces/robot";

class RobotCommand {
  // Function to move the robot forward
  moveForward(robot: Robot) {
    switch (robot.direction) {
      case "N":
        robot.y<99 && robot.y++;
        break;
      case "S":
        robot.y>0 && robot.y--;
        break;
      case "E":
        robot.x <99 && robot.x++;
        break;
      case "W":
        robot.x >0 && robot.x--;
        break;
    }
  }

  // Function to rotate the robot left
  rotateLeft(robot: Robot) {
    switch (robot.direction) {
      case "N":
        robot.direction = "W";
        break;
      case "S":
        robot.direction = "E";
        break;
      case "E":
        robot.direction = "N";
        break;
      case "W":
        robot.direction = "S";
        break;
    }
  }

  // Function to rotate the robot right
  rotateRight(robot: Robot) {
    switch (robot.direction) {
      case "N":
        robot.direction = "E";
        break;
      case "S":
        robot.direction = "W";
        break;
      case "E":
        robot.direction = "S";
        break;
      case "W":
        robot.direction = "N";
        break;
    }
  }

  // Function to process commands
  processCommands(robot: Robot, commands: string) {
    const commandArray: string[] = commands.split("");
    let currentCommand: string;
    let commandCount: number;

    while (commandArray.length > 0) {
      currentCommand = commandArray.shift() as string;
      let command=commandArray.shift();
      let checkNo=parseInt(command || "1", 10);
      if(!checkNo){
        commandCount=1;
        commandArray.unshift(String(command))
      }else{
        commandCount=checkNo
      }
      for (let i = 0; i < commandCount; i++) {
        switch (currentCommand as Command) {
          case "M":
            this.moveForward(robot);
            break;
          case "L":
            this.rotateLeft(robot);
            break;
          case "R":
            this.rotateRight(robot);
            break;
          default:
            break;
        }
      }
    }
  }

  // Function to read input from a file and return an array of lines
  readInputFromFile(filePath: string): string[] {
    try {
      const data = fs.readFileSync(filePath, "utf-8");
      return data.trim().split("\n");
    } catch (error: any) {
      console.error(`Error reading file: ${error.message}`);
      process.exit(1);
    }
  }
}

export const robotCommand = new RobotCommand();
