import { Robot } from "../interfaces/robot";
import { robotCommand } from "../dao/robot";

describe("RobotCommand Class", () => {
  let robot: Robot;

  beforeEach(() => {
    // Initialize a new robot before each test
    robot = {
      x: 0,
      y: 0,
      direction: "N",
    };
  });

  it("should move the robot forward correctly", () => {
    robotCommand.moveForward(robot);
    expect(robot).toEqual({ x: 0, y: 1, direction: "N" });
  });

  it("should rotate the robot left correctly", () => {
    robotCommand.rotateLeft(robot);
    expect(robot).toEqual({ x: 0, y: 0, direction: "W" });
  });

  it("should rotate the robot right correctly", () => {
    robotCommand.rotateRight(robot);
    expect(robot).toEqual({ x: 0, y: 0, direction: "E" });
  });

  it("should process commands correctly", () => {
    const commands = "M3R2L1";
    robotCommand.processCommands(robot, commands);
    expect(robot).toEqual({ x: 0, y: 3, direction: "E" });
  });
});
