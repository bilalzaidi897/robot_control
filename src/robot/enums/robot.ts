
export type Direction = 'N' | 'S' | 'E' | 'W';
export type Command = 'M' | 'L' | 'R';