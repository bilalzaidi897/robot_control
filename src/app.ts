import {robotCommand} from "./robot/dao/robot"
import { Robot } from './robot/interfaces/robot';
import {Direction} from "./robot/enums/robot";

// Main function(self executing function)
(function main() {
    const inputFile = 'input.txt'; // Replace with the actual path to your input file
    const inputLines = robotCommand.readInputFromFile(inputFile);
  
    // Parse the initial position
    const initialPosition = inputLines[0].split(' ');
    const initialX = parseInt(initialPosition[1], 10) || 0;
    const initialY = parseInt(initialPosition[2], 10) || 0;
    const initialDirection = initialPosition[0] as Direction;
    debugger
    // Create the initial robot
    const robot: Robot = {
      x: initialX,
      y: initialY,
      direction: initialDirection,
    };
    // Process commands
    robotCommand.processCommands(robot, inputLines[1]);
  
    // Output the final position
    console.log(`Final Position:${robot.direction} ${robot.x} ${robot.y}`);
  })();